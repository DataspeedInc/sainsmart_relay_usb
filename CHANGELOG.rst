^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package sainsmart_relay_usb
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.0 (2024-05-14)
------------------
* Migrate to ROS2
* Contributors: Gabe Oetjens
